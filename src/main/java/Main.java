import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import static com.sofia.view.Menu.runMenu;

public class Main {
    private static final Logger LOG = LogManager.getLogger(Main.class);

    public static void main(String[] args) {
        LOG.debug("START!");
        runMenu();
    }
}
