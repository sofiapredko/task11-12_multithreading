package com.sofia.model;

public class Fibonacci implements Runnable{
    public int countNumbers;
    public int result;

    public Fibonacci(int countNumbers) {
        this.countNumbers = countNumbers;
    }

    public int getFibonacciNumbers() {
        int firstFibNum = 0;
        int secondFibNum = 1;
        if (countNumbers < 1) {
            System.out.println("Wrong size of set");
            System.exit(0);
        } else if (countNumbers == 1) {
            System.out.println(countNumbers);
        }
        for (int i = 0; i < countNumbers; i++) {
            result +=secondFibNum;
            System.out.print(secondFibNum + " ");
            int nextFibNum = firstFibNum + secondFibNum;
            firstFibNum = secondFibNum;
            secondFibNum = nextFibNum;
        }
        System.out.println();
        return result;
    }

    @Override
    public void run(){
        getFibonacciNumbers();
    }

}


