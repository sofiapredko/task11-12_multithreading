package com.sofia.model;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class MultiFibonacci extends Thread {
    private static final Logger LOG = LogManager.getLogger(MultiFibonacci.class);
    private int numberInFibonacciSet;
    public int numberValue;

    public MultiFibonacci(int numberInFibonacciSet) {
        this.numberInFibonacciSet = numberInFibonacciSet;
    }

    public void run() {
        if (numberInFibonacciSet == 0) {
            numberValue = 0;
        } else if (numberInFibonacciSet == 1 || numberInFibonacciSet == 2) {
            numberValue = 1;
        } else {
            try {
                MultiFibonacci threadFind1 = new MultiFibonacci(numberInFibonacciSet - 1);
                MultiFibonacci threadFind2 = new MultiFibonacci(numberInFibonacciSet - 2);
                threadFind1.start();
                threadFind2.start();
                threadFind1.join();
                threadFind2.join();
                numberValue = threadFind1.numberValue + threadFind2.numberValue;
            } catch (InterruptedException ex) {
                LOG.info("Error!!! Can`t be interrupted!");
                ex.printStackTrace();
            }
        }
    }
}
