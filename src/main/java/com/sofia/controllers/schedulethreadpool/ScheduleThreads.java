package com.sofia.controllers.schedulethreadpool;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Random;
import java.util.concurrent.*;

public class ScheduleThreads {
    private static final Logger LOG = LogManager.getLogger(ScheduleThreads.class);
    private static final int MAX_RAND = 10;
    private static final int MIN_RAND = 1;
    private static final int CORE_POOL_SIZE = 3;

    public static Thread createThread() {
        Random rand = new Random();
        Thread runnableThread = new Thread() {
            @Override
            public void run() {
                int count = rand.nextInt((MAX_RAND - MIN_RAND) + 1) + MIN_RAND;
                try {
                    sleep(count);
                    LOG.info("I`ve sleeped " + count + " miliseconds");
                } catch (InterruptedException e) {
                    LOG.error("Error!!!");
                    e.printStackTrace();
                }
                System.out.println();
            }
        };
        return runnableThread;
    }

    public static void threadPoolSchedule() throws InterruptedException, ExecutionException {
        Thread runnableThread = createThread();
        ScheduledExecutorService fixed = Executors.newScheduledThreadPool(CORE_POOL_SIZE);
        fixed.schedule(runnableThread, 1, TimeUnit.SECONDS);
        fixed.schedule(runnableThread, 1, TimeUnit.SECONDS);
        fixed.schedule(runnableThread, 1, TimeUnit.SECONDS);
        fixed.shutdown();
    }


    public static void threadPoolFuture() {
        Thread runnableThread = createThread();
        ScheduledExecutorService fixed = Executors.newScheduledThreadPool(CORE_POOL_SIZE);
        ScheduledFuture<?> result = fixed.scheduleAtFixedRate(runnableThread, 1, 3, TimeUnit.SECONDS);
        try {
            TimeUnit.MILLISECONDS.sleep(20000);
        } catch (InterruptedException e) {
            LOG.error("Error!!!");
            e.printStackTrace();
        }
        fixed.shutdown();
    }

    public static void runThreadPools() throws ExecutionException, InterruptedException {
        threadPoolSchedule();
        threadPoolFuture();
    }
}

