package com.sofia.controllers.pipecommunication;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.PipedWriter;
import java.util.Scanner;

public class WriterThread implements Runnable {
    private static final Logger LOG = LogManager.getLogger(WriterThread.class);
    Scanner scan = new Scanner(System.in);
    PipedWriter pipeWriter;
    String name = "";

    public WriterThread(String name, PipedWriter pipedWriter) {
        this.name = name;
        this.pipeWriter = pipedWriter;
    }

    public void run() {
        try {
            while (true) {
                LOG.info("message :");
                String inputData = scan.nextLine();
                pipeWriter.write(inputData);
                pipeWriter.flush();
                System.out.println();
            }
        } catch (Exception e) {
            LOG.error("Exception with Pipes: : " + e);
        }
    }
}
