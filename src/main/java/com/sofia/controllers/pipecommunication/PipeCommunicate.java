package com.sofia.controllers.pipecommunication;

import java.io.PipedReader;
import java.io.PipedWriter;

public class PipeCommunicate {

    public PipeCommunicate() {
        try {
            PipedReader pipeRead = new PipedReader();
            PipedWriter pipeWrite = new PipedWriter();

            pipeWrite.connect(pipeRead);

            Thread thread1 = new Thread(new ReaderThread("ReaderThread", pipeRead));
            Thread thread2 = new Thread(new WriterThread("WriterThread", pipeWrite));

            thread1.start();
            thread2.start();

        } catch (Exception e) {
            System.out.println("PipeThread Exception: " + e);
        }
    }

    public static void runPipes() {
        new PipeCommunicate();
    }
}
