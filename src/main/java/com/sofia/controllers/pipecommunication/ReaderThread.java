package com.sofia.controllers.pipecommunication;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.PipedReader;

public class ReaderThread implements Runnable {
    private static final Logger LOG = LogManager.getLogger(ReaderThread.class);
    PipedReader pipeReader;
    String name = null;

    public ReaderThread(String name, PipedReader pr) {
        this.name = name;
        this.pipeReader = pr;
    }

    public void run() {
        try {
            while (true) {
                char c = (char) pipeReader.read();
                if (c != -1) {
                    System.out.print(c);
                }
                System.out.println();
            }
        } catch (Exception e) {
            LOG.error("Exception with Pipes: " + e);
        }
    }
}
