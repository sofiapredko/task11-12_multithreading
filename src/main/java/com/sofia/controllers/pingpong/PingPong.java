package com.sofia.controllers.pingpong;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import java.time.LocalDateTime;
import static java.lang.Thread.sleep;

public class PingPong {
    private static final Logger LOG = LogManager.getLogger(PingPong.class);
    private static Object syncMonitor = new Object();
    private static final int NUMBER_STEP = 10;
    private static final int SLEEP_TIME = 800;

    public static void showPing() {
        Thread pingThread = new Thread(() -> {
            synchronized (syncMonitor) {
                for (int i = 0; i < NUMBER_STEP; i++) {
                    try {
                        syncMonitor.wait();
                        sleep(SLEEP_TIME);
                        System.out.println("Ping");
                    } catch (InterruptedException e) {
                        LOG.error("Error with threads!!!");
                        e.printStackTrace();
                    }
                    syncMonitor.notify();
                }
            }
        });
        pingThread.start();
    }

    public static void showPong() {
        Thread pongThread = new Thread(() -> {
            synchronized (syncMonitor) {
                for (int i = 0; i < NUMBER_STEP; i++) {
                    syncMonitor.notify();
                    try {
                        syncMonitor.wait();
                        sleep(SLEEP_TIME);
                        System.out.println("Pong");
                    } catch (InterruptedException e) {
                        LOG.error("Error with threads!!!");
                        e.printStackTrace();
                    }

                }
            }
        });
        pongThread.start();
    }

    public static void run() {
        LOG.trace(LocalDateTime.now());
        LOG.debug("Start!!!");
        showPing();
        showPong();
    }
}
