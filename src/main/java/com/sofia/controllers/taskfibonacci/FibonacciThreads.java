package com.sofia.controllers.taskfibonacci;

import com.sofia.model.Fibonacci;
import com.sofia.model.MultiFibonacci;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class FibonacciThreads {
    private static final Logger LOG = LogManager.getLogger(FibonacciThreads.class);
    public static final int COUNT1 = 5;
    public static final int COUNT2 = 9;
    public static final int COUNT3 = 11;
    public static final int FIND_NUMBER_VALUE1 = 9;

    public static void runFirstMethod() {
        Fibonacci obj = new Fibonacci(COUNT1);
        Thread fib = new Thread(obj, "fibonacci");
        LOG.info(fib);
        fib.start();
    }

    public static void runSecondMethod() {
        Fibonacci obj2 = new Fibonacci(COUNT2);
        Thread fib2 = new Thread() {
            @Override
            public void run() {
                System.out.println();
                obj2.getFibonacciNumbers();
                LOG.info(obj2.getFibonacciNumbers());
            }
        };
        fib2.setName("Fib2");
        LOG.info(fib2);
        fib2.start();
    }

    public static void runThirdMethod() {
        Fibonacci obj3 = new Fibonacci(COUNT3);
        Thread fib3 = new Thread(() -> {
            LOG.info(Thread.currentThread().getName());
            obj3.getFibonacciNumbers();
            LOG.info(obj3.getFibonacciNumbers());
        }, "fib3");
        fib3.start();
    }

    public static void runMultiFibThread(int findNumberValue) {
        try {
            MultiFibonacci findFibonacci = new MultiFibonacci(findNumberValue);
            findFibonacci.start();
            findFibonacci.join();
            LOG.info("Fibonacci number at " + findNumberValue + " position is: " + findFibonacci.numberValue);
        } catch (Exception e) {
            LOG.error("Error!!!");
            e.printStackTrace();
        }
    }

    public static void runFibonacciFunctions() {
        runFirstMethod();
        runSecondMethod();
        runThirdMethod();
        runMultiFibThread(FIND_NUMBER_VALUE1);
    }

}
