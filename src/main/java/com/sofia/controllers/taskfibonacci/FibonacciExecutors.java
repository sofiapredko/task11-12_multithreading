package com.sofia.controllers.taskfibonacci;

import com.sofia.model.Fibonacci;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.concurrent.*;

import static com.sofia.controllers.taskfibonacci.FibonacciThreads.*;

public class FibonacciExecutors {
    private static final Logger LOG = LogManager.getLogger(FibonacciExecutors.class);

    public static void simpleExecutor() {
        Fibonacci testObj1 = new Fibonacci(COUNT1);
        Runnable myRunnable = () -> {
            Thread.currentThread().setName("Fibonacci simple executor");
            LOG.info(Thread.currentThread().getName());
            testObj1.getFibonacciNumbers();
            LOG.info(testObj1.getFibonacciNumbers());
        };
        Executor executor = Executors.newSingleThreadExecutor();
        executor.execute(myRunnable);
    }

    public static void serviceExecutor() {
        Fibonacci testObj2 = new Fibonacci(COUNT2);
        ExecutorService executorService = Executors.newSingleThreadExecutor();
        executorService.submit(() -> {
            testObj2.getFibonacciNumbers();
            LOG.info(testObj2.getFibonacciNumbers());
        });
        executorService.shutdown();
    }

    public static void futureExecutor() {
        ExecutorService executorService = Executors.newSingleThreadExecutor();
        Future<Integer> futureFibonacciSum = executorService.submit(() -> {
            Fibonacci testObj3 = new Fibonacci(COUNT3);
            return testObj3.getFibonacciNumbers();
        });
        try {
            LOG.info("Sum with Future = " + futureFibonacciSum.get());
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
            LOG.error("Error!!!");
        }
        executorService.shutdown();
    }

    public static void simpleCallable() throws Exception {
        Fibonacci testObj4 = new Fibonacci(COUNT1);
        ExecutorService executorService = Executors.newSingleThreadExecutor();
        Callable<Integer> task = () -> {
            try {
                TimeUnit.SECONDS.sleep(1);
                LOG.info(testObj4.getFibonacciNumbers());
                return testObj4.getFibonacciNumbers();
            } catch (InterruptedException e) {
                throw new IllegalStateException("task interrupted", e);
            }
        };
        task.call();
    }

    public static void runFibonacciExecutors() throws Exception {
        simpleExecutor();
        serviceExecutor();
        futureExecutor();
        simpleCallable();
    }
}

