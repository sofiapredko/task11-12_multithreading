package com.sofia.controllers.syncmethods;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class SynchronizeMethods {
    private static final Logger LOG = LogManager.getLogger(SynchronizeMethods.class);
    private static Object syncMonitor = new Object();
    private static long syncNumber = 0;
    private static long countScoreNumber = 5;
    private static long countScoreNumber2 = 3;

    public static void firstSyncMehod() {
        Thread firstThread = new Thread(() -> {
            synchronized (syncMonitor) {
                for (int i = 1; i <= countScoreNumber; i++) {
                    syncNumber++;
                    LOG.info(Thread.currentThread().getName() + " " + syncNumber);
                }
                LOG.debug("finish " + Thread.currentThread().getName());
            }
        });
        firstThread.start();
    }

    public static void secondSyncMehod() {
        Thread secondThread = new Thread(() -> {
            synchronized (syncMonitor) {
                for (int i = 1; i <= countScoreNumber2; i++) {
                    syncNumber--;
                    LOG.info(Thread.currentThread().getName() + " " + syncNumber);
                }
                LOG.debug("finish " + Thread.currentThread().getName());
            }
        });
        secondThread.start();
    }

    public static void thirdSyncMehod() {
        Thread thirdThread = new Thread(() -> {
            synchronized (syncMonitor) {
                syncNumber *= syncNumber;
                LOG.info(Thread.currentThread().getName() + " " + syncNumber);
                LOG.debug("finish " + Thread.currentThread().getName());
            }
        });
        thirdThread.start();
    }

    public static void runSyncMethods() {
        firstSyncMehod();
        secondSyncMehod();
        thirdSyncMehod();
    }
}
