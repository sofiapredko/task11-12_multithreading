package com.sofia.view;

import java.io.IOException;

@FunctionalInterface
public interface Functional {
    void start() throws Exception;
}