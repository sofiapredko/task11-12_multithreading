package com.sofia.view;

import com.sofia.controllers.pingpong.PingPong;
import com.sofia.controllers.pipecommunication.PipeCommunicate;
import com.sofia.controllers.schedulethreadpool.ScheduleThreads;
import com.sofia.controllers.syncmethods.SynchronizeMethods;
import com.sofia.controllers.taskfibonacci.FibonacciExecutors;
import com.sofia.controllers.taskfibonacci.FibonacciThreads;
import org.apache.logging.log4j.LogManager;

import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

import org.apache.logging.log4j.Logger;

final public class Menu {
    private Map<String, String> menu;
    private Map<String, Functional> methodsMenu;
    private static final Scanner INPUT = new Scanner(System.in);
    private static final Logger LOG = LogManager.getLogger(Menu.class);

    private Menu() {
        initMenu();
    }

    private void initMenu() {
        menu = new LinkedHashMap<>();
        methodsMenu = new LinkedHashMap<>();

        menu.put("1", " 1 - Ping Pong game with threads");
        menu.put("2", " 2 - Fibonacci sets with multithreading");
        menu.put("3", " 3 - Fibonacci sets with different types of Executors");
        menu.put("4", " 4 - Test ScheduledThreadPool");
        menu.put("5", " 5 - Test Synchronized methods on the same object");
        menu.put("6", " 6 - Test Pipe Communication");
        menu.put("0", " 0 - Exit.");

        methodsMenu.put("1", PingPong::run);
        methodsMenu.put("2", FibonacciThreads::runFibonacciFunctions);
        methodsMenu.put("3", FibonacciExecutors::runFibonacciExecutors);
        methodsMenu.put("4", ScheduleThreads::runThreadPools);
        methodsMenu.put("5", SynchronizeMethods::runSyncMethods);
        methodsMenu.put("6", PipeCommunicate::runPipes);
        methodsMenu.put("0", this::exitFromProgram);
    }

    private void outputMenu() {
        System.out.println("\nMENU:");
        menu.values().forEach(System.out::println);
    }

    private void exitFromProgram() {
        LOG.debug("SUCCESSFULLY END");
        System.exit(0);
    }

    public static void runMenu() {
        String key;
        Menu menu = new Menu();
        while (true) {
            try {
                System.out.println("\n");
                menu.outputMenu();
                System.out.println("Please, select menu point");
                key = INPUT.nextLine();
                System.out.println("\n\n");
                menu.methodsMenu.get(key).start();
            } catch (NullPointerException e) {
                LOG.warn("\nChoose correct choice please");
            } catch (IOException e) {
                LOG.error("\nSomething wrong with files!");
            } catch (ClassNotFoundException e) {
                LOG.error("\nCan't find class");
            } catch (Exception e) {
                LOG.error("ERROR!!!");
            }
        }
    }
}